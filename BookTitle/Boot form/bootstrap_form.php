<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../asset/bootstrap/css/bootstrap.min.css">
    <script src="../../asset/bootstrap/js/jquery.min.js"></script>
    <script src="../../asset/bootstrap/js/bootstrap.min.js"></script>
</head>
<body>

<div align="center" class="container-fluid">
    <h2 style="color: #0f0f0f">Book Information</h2>
    <form>
        <div class="form-group">
            <label for="textfield">Book Title:</label>
            <input style="background-color: #5bc0de" type="text" class="form-control" id="name" placeholder="Enter book name">
        </div>
        <div class="form-group">
            <label for="textfield">Author name</label>
            <input style="background-color: #2aabd2" type="text" class="form-control" id="author" placeholder="Enter authors name">
        </div>
        <div class="form-group">
            <label  for="publication year">Year of publication</label>
            <input style="background-color: #9acfea" type="text" class="form-control" id="publication year" placeholder="Enter publications year">
            </div>

        <button style="color: #3e8f3e" type="submit" class="btn btn-default">Submit</button>
    </form>
</div>

</body>
</html>
